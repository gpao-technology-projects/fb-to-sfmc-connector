/**
 * Made for GPAo💚 by Altan
 * FB Lead Data to SFMC DE via API
 * - Uses FB Webhook to get LeadID
 * - FB Marketing API to lookup Name,Email,Number with LeadID
 * - SFMC API to Push Name,Email,Number to a Data Extension
 *
 * Dev Notes:
 * https://developers.facebook.com/docs/marketplace/vehicles/retrieving-leads/
 * https://developers.facebook.com/docs/graph-api/webhooks/getting-started/webhooks-for-leadgen
 *
 * FB APP:
 * - Must be added to a FB Page for Debug Tools to work
 * - Must request App review for Increased Permissions
 *
 */
"use strict";

const bodyParser = require("body-parser");
const express = require("express");
const crypto = require("crypto"); //Used for randomID generation
const parsePhoneNumber = require("libphonenumber-js/max"); //Using Max variant for isType() function
const app = express();
const axios = require("axios"); // Used to POST data to SFMC DE / API
const xhub = require("express-x-hub");
const bizSdk = require("facebook-nodejs-business-sdk"); // Facebook Business SDK
const Lead = bizSdk.Lead; // Facebook Business SDK for Leads
const token = process.env.TOKEN || "<TOKEN>"; //Used to Auth Webhook Connection
const access_token = process.env.ACCESS_TOKEN || "<ACCESS_TOKEN>"; // Used to Auth Facebook API Connection
const account_id = process.env.ACCOUNT_ID || "<ACCOUNT_ID>";
const app_secret = process.env.APP_SECRET || "<APP_SECRET>";
const app_id = process.env.APP_ID || "<APP_ID>";
const api = bizSdk.FacebookAdsApi.init(access_token); //Initialise Facebook API Connection
const deUrl = process.env.DE_URL;
const slackURL = process.env.SLACK_URL;
const slackToken = process.env.SLACK_TOKEN;

const showDebugingInfo = false; // Setting this to true shows debugging info.

if (showDebugingInfo && !process.env.PORT) {
  console.log(`DEBUG .env not found!`);
} // Debug for CloudBuild and general missing .env

// Initialize all the things 💚
let sfmcSecretToken,
  fields,
  params,
  leadIdArray,
  formIdArray,
  leadFullFieldData;

sfmcSecretToken = ""; //Used to store generated AccessToken
fields = [];
params = {};
leadIdArray = []; // Used in storeLeadIds function to store LeadIDs
formIdArray = []; // Store FormId - used to identify which from the lead comes from
leadFullFieldData = {
  items: [
    {
      firstname: "",
      lastname: "",
      phone: "",
      "email opt-in": "true",
      campaign: "",
      transformed_phone: "",
      mobile: "",
      ID: "",
      email: "",
      utm_campaign: "",
      utm_content: "",
      utm_medium: "",
      utm_source: "",
      utm_term: "",
      data_source: "facebook",
      page_code: "",
      landline: "",
    },
  ],
}; // Store Lead Info from API lookup

// // Salesforce Marketing Cloud Auth SDK
const FuelAuth = require("fuel-auth");

//Options for SFMC Get Auth Token (these expire in 20min)
const FuelAuthClient = new FuelAuth({
  authOptions: { authVersion: 2 },
  clientId: process.env.CLIENT_ID,
  clientSecret: process.env.CLIENT_SECRET,
  authUrl: process.env.AUTH_URL,
});

if (showDebugingInfo) {
  api.setDebug(true);
}

// Send Message to Slack Channel
const slackMessage = async function (message) {
  if (typeof message === "undefined") {
    if (showDebugingInfo) {
      console.log(`DEBUG: no message text`);
    }
    return;
  } else {
    const res = await axios
      .post(
        slackURL,
        {
          attachments: [
            {
              color: "danger",
              text: message,
            },
          ],
        },
        { headers: { authorization: `Bearer ${slackToken}` } }
      )
      .then((res) => {
        console.log(`Slack POST status: ${res.data}`);
      })
      .catch((error) => {
        console.error(`Slack POST Error: ${error}`);
      });
  }
};

// Clean Data before storing (do not run on email value as it wil remove @)
const cleanData = function (value) {
  let cleanValue = value.replace("&", "and").replace(/[!@#$%^*()_+=<>]/g, "");
  return cleanValue;
};

const formatPhoneNumber = function (input) {
  if (showDebugingInfo) {
    console.log(`fomatPhoneNumber Input = ${input}`);
  }
  const phone = {
    phone_number: "",
    transformed_phone: "",
    mobile: "",
    landline: "",
  };
  try {
    const phoneNumber = parsePhoneNumber(input, "NZ");
    if (typeof phoneNumber == "undefined") {
      if (showDebugingInfo) {
        console.log("phoneNumber is undefined");
      }
      return phone;
    } else {
      if (phoneNumber.isValid() && phoneNumber.country === "NZ") {
        const numberType = phoneNumber.getType(); //Used to Determine if number is mobile or landline
        const cleanNumber = cleanData(phoneNumber.number); //Strip + and other symbols

        switch (numberType) {
          case "MOBILE":
            phone.phone_number = phoneNumber.number; //User typed number
            phone.mobile = phone.transformed_phone = cleanNumber; //Set both Mobile & Transformed_phone
            break;
          case "FIXED_LINE" || "UAN":
            phone.phone_number = phoneNumber.number; //User typed number
            phone.landline = phone.transformed_phone = cleanNumber; //Set both landline & transformed_phone
            break;
          default:
            if (showDebugingInfo) {
              console.log(`No match for: ${phoneNumber.number}`);
            }
            slackMessage(
              `DEBUG: Number Did not Match Mobile or Landline ${phoneNumber.number}`
            );
            break;
        }
        //DEBUG
        if (showDebugingInfo) {
          console.log(`phone_number = ${phone.phone_number}`);
          console.log(`transformed_phone = ${phone.transformed_phone}`);
          console.log(`phone.mobile = ${phone.mobile}`);
          console.log(`phone.landline = ${phone.landline}`);
          console.log(phoneNumber.country);
          console.log(phoneNumber.isValid());
        }
        return phone; //Returns phone Object
      } else {
        return phone;
      }
    }
  } catch (error) {
    if (showDebugingInfo) {
      console.log(error);
    }
    return phone;
  }
};

// Random ID 13 char length Generated with Crypto Module
const randomId = function () {
  return crypto.randomBytes(13).toString("hex");
};

// SFMC REST API Insert payload into FB Data Extension
const postLead = function (token) {
  if (showDebugingInfo) {
    console.log(`DEBUG: postLead: token = ${token}`);
  }
  axios
    .post(deUrl, leadFullFieldData, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
    .then((res) => {
      console.log(`POST status: ${res.status} ${res.statusText}`);
    })
    .catch((error) => {
      console.error(
        `POST Error: ${error.response.status} ${error.response.statusText}`
      );
      slackMessage(
        `POST Error: ${error.response.status} ${error.response.statusText}`
      );
    });
};

// SFMC Get Token for use later
const getToken = async function () {
  if (showDebugingInfo) {
    console.log(`DEBUG: Is token Expired: ${FuelAuthClient.isExpired()}`);
  }
  if (!FuelAuthClient.isExpired() && sfmcSecretToken !== "") {
    if (showDebugingInfo) {
      console.log(`DEBUG: Token is still valid, using saved token`);
    }
    postLead(sfmcSecretToken); //Using already saved Token
  } else {
    sfmcSecretToken = await FuelAuthClient.getAccessToken()
      .then(function (data) {
        // sfmcSecretToken = data.access_token; //Documentation says with _ but this fails on 2nd request
        sfmcSecretToken = data.accessToken; //Store the returned accesstoken
        if (showDebugingInfo) {
          // console.log(
          //   `DEBUG: GetAccessToken = ${JSON.stringify(data, null, 4)}`
          // );
          console.log(`DEBUG: sfmcSecretToken = ${sfmcSecretToken}`);
        }
        return sfmcSecretToken;
      })
      .catch(function (error) {
        console.log(error);
        slackMessage(JSON.stringify(error));
      });
    postLead(sfmcSecretToken); // using Token Upsert the Lead Data to SFMC via API
  }
};

// Using leadgen_id get data points and store in leadFieldData Array
const getFullLeadInfo = function (leadId, formId) {
  const leadResp = new Lead(leadId)
    .get(fields, params)
    .then((leadResp) => {
      for (let i = 0; i < leadResp.field_data.length; i++) {
        switch (leadResp.field_data[i].name) {
          case "first_name":
            leadFullFieldData.items[0].firstname = cleanData(
              leadResp.field_data[i].values[0]
            );
            break;
          case "last_name":
            leadFullFieldData.items[0].lastname = cleanData(
              leadResp.field_data[i].values[0]
            );
            break;
          case "phone_number":
            let leadPhone = formatPhoneNumber(leadResp.field_data[i].values[0]);
            leadFullFieldData.items[0].phone = leadPhone.phone_number;
            leadFullFieldData.items[0].mobile = leadPhone.mobile;
            leadFullFieldData.items[0].transformed_phone =
              leadPhone.transformed_phone;
            leadFullFieldData.items[0].landline = leadPhone.landline;
            break;
          case "campaign":
            leadFullFieldData.items[0].campaign =
              leadResp.field_data[i].values[0];
            break;
          case "email":
            leadFullFieldData.items[0].email = leadResp.field_data[i].values[0];
            break;
          case "utm_campaign":
            leadFullFieldData.items[0].utm_campaign =
              leadResp.field_data[i].values[0];
            break;
          case "utm_content":
            leadFullFieldData.items[0].utm_content =
              leadResp.field_data[i].values[0];
            break;
          case "utm_medium":
            leadFullFieldData.items[0].utm_medium =
              leadResp.field_data[i].values[0];
            break;
          case "utm_source":
            leadFullFieldData.items[0].utm_source =
              leadResp.field_data[i].values[0];
            break;
          case "utm_term":
            leadFullFieldData.items[0].utm_term =
              leadResp.field_data[i].values[0];
            break;
          // case "data_source":
          //   leadFullFieldData.items[0].data_source =
          //     leadResp.field_data[i].values[0];
          //   break;
          case "page_code":
            leadFullFieldData.items[0].page_code =
              leadResp.field_data[i].values[0];
            break;
          default:
            console.log(
              `INFO: Some data does not match defined fields/attributes`
            );
            break;
        }
      }

      leadFullFieldData.items[0].ID = randomId(); //Generates new random ID

      //DEBUG
      if (showDebugingInfo) {
        console.log(`DEBUG: getFullLeadInfo - leadResp =`);
        console.log(leadResp.field_data);
        console.log(leadFullFieldData.items[0]);
      }
      getToken(); //SFMC getToken function (only run if api lookup is successfull)
    })
    .catch((error) => {
      console.log(error);
      let errorMessage = JSON.stringify(error.message);
      errorMessage += leadId;
      slackMessage(errorMessage);
      return;
    });
};

// Store Webhook LeadIds into array
const storeLeadIds = function (responsePayload) {
  // Itterate through all leads and parse the leadgen ID on to getLeadInfo Function
  // TODO: May need to modify the .length not sure under what array or object the additional lead ids will come through without testing
  for (let i = 0; i < responsePayload.entry.length; i++) {
    leadIdArray[i] = responsePayload.entry[i].changes[i].value.leadgen_id;
    formIdArray[i] = responsePayload.entry[i].changes[i].value.form_id; // TODO: Improve this by using an object over two arrays
    //DEBUG
    if (showDebugingInfo) {
      console.log(`DEBUG: LeadId = ${leadIdArray[i]}`);
      console.log(`DEBUG: FormId = ${formIdArray[i]}`);
    }
    getFullLeadInfo(leadIdArray[i], formIdArray[i]); //On every Loop call FB API to get Lead Info
  }
};

// Listen for Webhook Ingest
const port = process.env.PORT || 3001;
app.listen(port, () => console.log(`🚀 Server running on port ${[port]}`));

// Process APP_Secret for later Verification
app.use(xhub({ algorithm: "sha1", secret: app_secret }));
app.use(bodyParser.json());
// app.use(express.json()); //Maybe can replace bodyParser with express

// Root/Index get response
app.get("/", function (req, res) {
  res.redirect("https://www.greenpeace.org/global/"); // nothing here so redirect to a default page to deter
});

// Validate request is from FB and passes token check as expected
app.get(["/facebook"], function (req, res) {
  if (
    req.query["hub.mode"] == "subscribe" &&
    req.query["hub.verify_token"] == token
  ) {
    res.send(req.query["hub.challenge"]);
  } else {
    // res.sendStatus(400);
    res.redirect("https://www.greenpeace.org/global/"); // nothing here so redirect to a default page to deter
  }
});

// Recieve Post Data from Facebook Webhook and check if req is Valid
app.post("/facebook", function (req, res) {
  if (!req.isXHubValid()) {
    console.log(
      "Warning - request header X-Hub-Signature not present or invalid"
    );
    // res.sendStatus(401);
    res.redirect("https://www.greenpeace.org/global/"); // nothing here so redirect to a default page to deter
    return; // Ends function
  }
  // DEBUG
  if (showDebugingInfo) {
    console.log("DEBUG: request header X-Hub-Signature validated");
    console.log("DEBUG: Facebook request body:", req.body);
  }
  storeLeadIds(req.body); // Triggers storage and API lookup
  res.sendStatus(200); // Must return 200 status to webhook
});

app.listen();
